

-- 1 poder segmentar por distintos correlativos
select cod_factura from tb_fact_factura where fecha_emision='2020-05-07'and numero_documento like('FYVZ-00000%');

-- 2 poder segmentar por nro documento
select numero_documento,cod_factura from tb_fact_factura where fecha_emision='2020-05-07'and numero_documento like('BYVZ-000000%');

--3 poder identificar los cod_factura para poder armar el update
select * from tb_fact_factura_detalle where cod_factura in(880281) and not cod_paquete in (615);

-- Ejemplos que no deben ser tomados en cuenta
-- 1717373 descripcion Alquiler Servicio de 1 DID

--4 tener siempre desactivado la siquiente linea,procesar lo que siempre pide papa oso
--update tb_fact_factura_detalle set descripcion=CONCAT('Servicio valor agregado',descripcion) where cod_factura=880299 and not cod_paquete in(615);

--5 generar los pdf correctos

--6 actualizar manualmente los correlativos FYVZ y BYVZ para evitar problemas en el siguiente proceso.

--7 remitir via email los pdf generados a telefonica para su validción.

